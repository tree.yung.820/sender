import axios from "axios";

type CV = {
	name: string;
	email: string;
	position: string;
	cv_url: string;
	cover_letter_url?: string;
	intro?: string;
	website?: string;
	note?: string;
	code_url?: string;
};
const cvData: CV = {
	name: "Yung Ho Kei, David",
	email: "tree.yung.820@gmail.com",
	position: "Web Developer (Full Time)",
	cv_url: "https://bit.ly/3RgSVcx",
	note: "front-end secret1 :HakunaMatataSimbaBMW, userUUID: 'cl78o0h1k0000pn26vzvhggc7'",
	website: "https://bit.ly/3AmDjxm",
	code_url: "https://gitlab.com/tree.yung.820/sender",
};

axios
	.post("http://career.wemine.hk/cv-submit", cvData)
	.then((response) => console.log(response))
	.catch((error) => console.log(error));
